import numpy as np
from firedrake import *
import matplotlib
from matplotlib import pylab as plt
from figure_settings import pgf_with_latex
import ast
import os

import sys
sys.path.append("../solver/")
from misc import x_sorted_contact_indices, compute_sliding_law_point, find_paths

# Problems under consideration
effpres = [2.23, 1.88]
wl = [2.5, 2.5]
tstart = [5,5]

amp = 0.08
ng = 3
a = 0.1
N = 192
dt = 0.005
T = 10

# Settings
Nplots = len(effpres)
matplotlib.rcParams.update(pgf_with_latex(3))

# define bed shape
xbed = np.linspace(0,1,N+1)
bed = -amp * (1 - np.sin(0.5*np.pi - 2*np.pi*xbed))
ntheta = 5

###########################
# Obtain data for steady state friction curve
###########################

Nss = 192
run_dir = "../data/n%d_amp%.3f_N%d/" % (ng, amp, Nss)

# Read info
ep_ss = np.loadtxt(run_dir + "/effpres.txt")
f = open(run_dir + "/run_info.txt", "r")
contents = f.read()
run_info = ast.literal_eval(contents)
f.close()
Ny = run_info["Ny"]

# Build sliding law
mesh = PeriodicRectangleMesh(Nss, Ny, 1, 1, direction = "x")
V = VectorFunctionSpace(mesh, "CG", 2)
M = FunctionSpace(mesh, "DG", 0)
u = Function(V)
lamda = Function(M)

_, paths = find_paths(run_dir, Nss)
ub = np.zeros(len(ep_ss))
taub = np.zeros(len(ep_ss))
theta_ss = []
for j, pathj in enumerate(paths):
    theta_vec = np.loadtxt(run_dir + "/" + pathj + "/theta.txt")
    theta_ss.append(theta_vec[-1])
    taub[j], ub[j], _, _ = compute_sliding_law_point(run_dir + pathj, mesh, u, lamda)

data_ss = np.zeros([len(ub), 2])
data_ss[:,0] = ub/ep_ss**ng
data_ss[:,1] = taub/ep_ss


###########################
# Read unsteady state info
###########################
panel_label = []
import string
alphabet = np.array(list(string.ascii_lowercase))
for i in range(Nplots):
    ind_i = np.array(range(3)) + 3*i
    panel_label.append(alphabet[ind_i])

colors0 = ["blue", "black", "green"]
fig1, ax1 = plt.subplots(3, Nplots)
plt.subplots_adjust(wspace = 0.35, hspace = 0.6)

for i in range(Nplots):

    path = "../output/unsteady_n%d_N%.2f_Nmesh%d_dt%.5f_wl%.2f_a%.2f" % (ng, effpres[i], N, dt, wl[i], a)

    ub_us = np.loadtxt(path + "/ub.txt")
    tau_us = np.loadtxt(path + "/tau.txt")
    cv = np.loadtxt(path + "/volume.txt")
    time_steps = int(float(T)/dt) + 1
    t = dt * np.array(range(time_steps))
    effpres_us =  np.loadtxt(path + "/effpres.txt")# effpres * (1 + a * np.sin(2 * np.pi / wl * t))
    theta_us = np.loadtxt(path + "/theta.txt")

    ind = np.argmin(abs(ep_ss-effpres[i]))

    print("ub0/N0^n = %.4f" % (ub[ind]/ep_ss[ind]**ng))

    # Prepare colors
    tplot = np.linspace(tstart[i],tstart[i] + wl[i], int(wl[i]/dt))
    ind_plot = np.array(tplot/dt, dtype = "int")
    norm = plt.Normalize()
    colors = plt.cm.plasma(norm(tplot/max(tplot)))
    ind_theta = np.linspace(0,colors.shape[0],ntheta+1)
    ind_theta = np.array(ind_theta[:-1], dtype = "int")

    # first plot
    ax1[0,i].plot(t, ub_us/ub[ind], color = colors0[0])
    ax1[0,i].plot(t, effpres_us/ep_ss[ind], color = colors0[1])
    ax1[0,i].set_xlim([0,T])
    ax1[0,i].set_xlabel("$t$")
    ax1[0,i].set_ylim([0.8, 1.5])

    ax12 = ax1[0,i].twinx()
    ax12.plot(t, cv/amp, color = colors0[2])
    ax12.set_ylim([0,0.33])
    # ax12.set_ylabel("$\\frac{V}{rL}$", rotation = 0)
    # ax12.yaxis.set_label_coords(1.25, 0.62)

    # Set x ticks
    ax1[0,i].set_xticks(np.linspace(0,10,5), minor=False)
    ax1[0,i].set_xticks(np.linspace(0,10,17), minor=True)
    ax1[0,i].xaxis.grid(True)
    ax1[0,i].xaxis.grid(which='minor', linestyle='--')

    # ax1[0,i].set(xticks = np.linspace(0,10,17))
    # temp = ax1[i,0].xaxis.get_ticklabels()
    # temp = list(set(temp) - set(temp[::4]))
    # for label in temp:
    #     label.set_visible(False)

    # Second plot
    ax1[1,i].plot(data_ss[:,0], data_ss[:,1], color = "black")
    ax1[1,i].yaxis.set_label_coords(-0.22, 0.465)
    ax1[1,i].set_xlim([0,0.62])
    # ax1[1,i].set_xlabel("$\\frac{u_b}{2ALN^n}$")
    ax1[1,i].set_xlabel("$u_b/(2ALN^n)$")

    # Add color points
    for (j,c) in enumerate(colors):
        ax1[0,i].plot(tplot[j],ub_us[ind_plot[j]]/ub[ind], color = c, marker = "o", markersize = 2)
        ax1[1,i].plot(ub_us[ind_plot[j]]/effpres_us[ind_plot[j]]**ng,tau_us[ind_plot[j]]/effpres_us[ind_plot[j]], color = c, marker = "o", markersize = 2)

    # Third plot
    for j in ind_theta:
        ax1[2,i].plot(xbed, theta_us[ind_plot[j]], color = colors[j])

    ax1[2,i].plot(xbed, bed, color = "black")
    ax1[2,i].plot(xbed, theta_ss[ind], color = "black", linestyle = ":")
    ax1[2,i].set_xlim([0,1])
    ax1[2,i].set_ylim([-2.1 * amp, 0.1*amp])
    ax1[2,i].set_xlabel("$x$")

    # Remove x axis labels and
    if i == 0:
        ax1[1,i].set_ylabel('$\\frac{\\tau_b}{N}$',rotation=0)

    # set labels
    ax1[0,i].set_title("$(u_0,N_0) = (%d, %.2f)$" % (1, ep_ss[ind]), fontsize = 8, loc='center')
    for j in range(3):
        ax1[j,i].set_title("(" + panel_label[i][j] + ")", fontsize = 8, loc='left')


# Add legend
from matplotlib.lines import Line2D
lines = [Line2D([0], [0], color=c) for c in colors0]
labels = ['$u_b/u_{b,0}$', "$N/N_{0}$", "$V/(rL^2)$"]
ax1[0,0].legend(lines, labels, bbox_to_anchor=(0.97, 1.15, 0.5, 0.5), ncol = 3, fontsize = 8)
# ax1[0,1].legend(lines, labels, bbox_to_anchor=(-1, 1, 0.5, 0.5), ncol = 1, fontsize = 8)
fig1.savefig("unsteady_1.pdf", bbox_inches='tight', pad_inches=0.03)
