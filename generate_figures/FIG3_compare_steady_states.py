from firedrake import *
import numpy as np
import matplotlib.pylab as plt
import matplotlib
from figure_settings import pgf_with_latex
matplotlib.rcParams.update(pgf_with_latex(2, hscale = 0.6))

import sys
sys.path.append("../solver/")
from misc import x_sorted_contact_indices, compute_sliding_law_point

### Steady results
N = [16,32,64,128,192]
effpres = 0.3
ng = 1
amp = 0.01
dt = [0.05, 0.025, 0.01, 0.005, 0.005]
A = 0.5

def bed(x):
    return -amp * (1 - np.sin(0.5*np.pi - 2*np.pi*x))

# Prepare plots
fig, ax = plt.subplots(2,1)
mrk = ["o", "s", "^","v", "x"]

# Prepare data file
ep_str = str(int(effpres * 100)).zfill(3)
txt_file = open("Table1.txt", "w")
txt = "$n_e$ & mesh cells & $\\tau_b$ & u_b & detachment & reattachment \\\\"
txt_file.write(txt + "\n")

for (i,Ni) in enumerate(N):

    print("N = %d" % Ni)

    # Build mesh, function spaces and find paths
    Ny = max([int(0.1*Ni), 3])
    mesh = PeriodicRectangleMesh(Ni, Ny, 1, 1, direction = "x")
    V = VectorFunctionSpace(mesh, "CG", 2)
    M = FunctionSpace(mesh, "DG", 0)
    u = Function(V)
    lamda = Function(M)

    # Calculate points
    path = "../output/Nmesh%d_n%d_amp%.2f_N%.2f_dt%.4f" % (Ni, ng, amp, effpres, dt[i])
    taub, ub, cpnt, dpnt = compute_sliding_law_point(path, mesh, u, lamda, bed)

    # Plot theta
    indU2Ub, x = x_sorted_contact_indices(mesh.coordinates.function_space(), 1)
    y = mesh.coordinates.dat.data[indU2Ub, 1]
    ax[0].plot(x,(y + amp)/(amp), linewidth = 1.0, label = "$n_e = %d$" % Ni)

    # Plot tau
    indMb2M, _ = x_sorted_contact_indices(M, 1)
    tau = lamda.dat.data[indMb2M]
    ytau = -tau/amp/effpres
    # ytau = np.append(ytau,ytau[0])
    xmid = 0.5 * (x[:-1] + x[1:])
    ax[1].plot(xmid, ytau, marker = mrk[i], markersize = "3", linewidth = 0.5, label = "$n_e = %d$" % Ni, zorder=(len(N) + 1-i)*5)

    # Add data
    Nmesh = mesh.num_cells()
    cc = min([cpnt, dpnt])
    dd = max([cpnt,dpnt])
    txt = "%d & %d & %.6f & %.5f & %.4f & %.4f \\\\" % (Ni, Nmesh, taub, ub, cc, dd)
    txt_file.write(txt + "\n")

txt_file.close()

# Prepare plots

# analytical_results: load them from provided database
ubonN_a = np.loadtxt("../data/linearised_results/ubonN.txt")
cpnt_a = np.loadtxt("../data/linearised_results/cs.txt")
dpnt_a = np.loadtxt("../data/linearised_results/ds.txt")
x_analytical = np.linspace(0,1,700)

from analytical_stress import stress_analytical
sigmaonub, x_analytical = stress_analytical(x_analytical,A,ub*amp,effpres,cpnt_a,dpnt_a,ubonN_a)

if min(x_analytical) < 0:
    inda = np.where(x_analytical < 0)
    x_analytical[inda] = x_analytical[inda] + 1
    inda = np.argsort(x_analytical)
    x_analytical = x_analytical[inda]
    sigmaonub = sigmaonub[inda]

if max(x_analytical) > 1:
    inda = np.where(x_analytical > 1)
    x_analytical[inda] = x_analytical[inda] - 1
    inda = np.argsort(x_analytical)
    x_analytical = x_analytical[inda]
    sigmaonub = sigmaonub[inda]


ax[1].plot(x_analytical, sigmaonub * ub/effpres, color = "black", zorder=0, label = "Exact linear")
ax[1].legend(loc = 1)
ax[1].set_xlim([0.6,1])
ax[1].set_ylim([-100,2000])
ymax = max(ytau)
ax[1].set_xlabel("$x/L$")
ax[1].set_ylabel("$-\\frac{\\sigma_{nn} + p_w}{Nr}$", rotation=0)
ax[1].yaxis.set_label_coords(-0.15,0.5)

xbed = np.linspace(0,1,200)
ybed = bed(xbed)
ax[0].plot(xbed,(ybed + amp)/amp, linewidth = 1.0, color = "black", zorder = 0)
ymin = min((ybed + amp)/amp) * 1.1; ymax = max((ybed + amp)/amp) * 1.1
ax[0].plot([0.6, 0.6], [ymin,ymax], color = "black", linestyle = "--", linewidth = 0.5)
ax[0].plot([1, 1], [ymin,ymax], color = "black", linestyle = "--", linewidth = 0.5)
ax[0].set_ylabel("$\\frac{y}{rH}$", rotation=0)
ax[0].set_xlabel("$x/L$")
ax[0].set_ylim([ymin, ymax])
ax[0].yaxis.set_label_coords(-0.1,0.5)

fig.savefig("Figure3.pdf", bbox_inches='tight', pad_inches=0.03)
