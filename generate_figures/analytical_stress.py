from firedrake import *
import numpy as np

def stress_analytical(xall,A,ub,N,c_pnts,d_pnts,ubonN_pnts):

    displace = False
    eta = 2 * A

    # Find c and d closest to Nonub
    ind = np.argmin(np.abs(ubonN_pnts - ub/N))
    c = c_pnts[ind] % 1
    d = d_pnts[ind]

    if c > d:
        d = d + 1
        xall = xall + c/2.
        displace = True

    assert np.isnan(c) == False, "No cavitation at this point"
    print("closest point to %.4e is %.4e, with c = %.4f and d = %.4f" % (ub/N, ubonN_pnts[ind], c, d))

    # Calculate stress
    sigma = np.zeros(xall.size)
    indx = np.where((xall > c) & (xall<d))
    x = xall[indx]
    alpha1 = 0.5 * np.pi * (3 * d + c)
    alpha2 = 0.5 * np.pi * (d-c)
    Nd = 8 * np.pi**2 * ub * eta* np.sin(alpha1) * np.sin(alpha2)

    beta1 = np.sin(np.pi * (2*x  + 0.5*(d-c) )) * np.sin(np.pi*(d-x)) -\
        np.sin(np.pi*(2*x - 0.5*(d-c))) * np.sin(np.pi*(x-c))
    beta2 = np.cos(np.pi*(x - 0.5*(c+d)))
    beta3 = np.abs(np.sin(np.pi*(d-x)) * np.sin(np.pi*(x-c)))**0.5

    sigma[indx] = (4 * np.pi**2 * ub * eta * beta1 + Nd * beta2) / beta3

    return sigma/ub, xall - 0.25

if __name__ == "__main__":
    # Nonub = 24.43
    N = 0.3
    ub = 0.009857725854160155#N/Nonub
    A = 0.5
    xall = np.linspace(0,1,200)

    # Read analytical data
    Path = "../data/linearised_results"
    ubonN_a = np.loadtxt(Path + "/ubonN.txt")
    cpnt_a = np.loadtxt(Path + "/cs.txt")
    dpnt_a = np.loadtxt(Path + "/ds.txt")

    sigmaonub, xall = stress_analytical(xall,A,ub,N,cpnt_a,dpnt_a,ubonN_a)

    if min(xall) < 0:
        ind = np.where(xall < 0)
        xall[ind] = xall[ind] + 1
        inda = np.argsort(xall)
        xall = xall[inda]
        sigmaonub = sigmaonub[inda]

    if max(xall) > 1:
        ind = np.where(xall > 1)
        xall[ind] = xall[ind] - 1
        inda = np.argsort(xall)
        xall = xall[inda]
        sigmaonub = sigmaonub[inda]

    import pdb; pdb.set_trace()

    # Plot
    from matplotlib import pylab as plt
    fig = plt.figure()
    plt.plot(xall,sigmaonub, label = "new")

    # plt.ylim([0,max(sigmaonub)])

    # sigmaonub2 = np.loadtxt("output/analytical/Nonub24.43_sigmaonub.txt")
    # x2 = np.loadtxt("output/analytical/Nonub24.43_x.txt")
    # plt.plot(x2,sigmaonub2, label = "old")
    # plt.legend()

    fig.savefig("stress.png")
