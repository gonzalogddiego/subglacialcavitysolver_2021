from firedrake import *
import numpy as np
import matplotlib.pylab as plt
import ast
import matplotlib
from figure_settings import pgf_with_latex
matplotlib.rcParams.update(pgf_with_latex(1, hscale = 1.1))

import sys
sys.path.append("../solver/")
from misc import x_sorted_contact_indices, compute_sliding_law_point, find_paths


# numerical data
ng = [1, 1, 1, 3, 3, 3, 5, 5, 5]
N = [192] * len(ng)
amp = [0.01, 0.04, 0.08, 0.01, 0.04, 0.08, 0.01, 0.04, 0.08]
utop = 1
c0 = [1.0014, 1.0014, 1.0014, 0.3434, 0.3434, 0.3434, 0.1255, 0.1255, 0.1255]
analytical = True

fig, ax = plt.subplots(1,2)

mrk = ["o", "s", "^", "o", "s", "^", "o", "s", "^"]
mew_list = [2,1]

ylabel_law = "$\\frac{\\tau_b}{rN}$"
xlabel_law = "$\\left(\\frac{\\alpha(n) r}{A L}\\right)^{1/n} \\frac{u_b^{1/n}}{N}$"
ylabel_endp = "$\\frac{x}{L}$"
xlabel_endp = xlabel_law

# Define color maps
pnts = [0.3,0.5,0.7]
colors = np.zeros([len(ng), 4])
colors[:3,:] = plt.cm.Greens(pnts)
colors[3:6,:] = plt.cm.Blues(pnts)
colors[6:9,:] = plt.cm.Reds(pnts)

marker = ["o", "s", "^"]

color_markers_1 = np.zeros([3, 4])
color_markers_1[0,:] = plt.cm.Greens(pnts[1])
color_markers_1[1,:] = plt.cm.Blues(pnts[1])
color_markers_1[2,:] = plt.cm.Reds(pnts[1])
color_markers_2 = plt.cm.Greys(pnts)


lw = 1
if analytical:
    lw = 0

from matplotlib.lines import Line2D
lines = [Line2D([0], [0], color = color_markers_2[i], linewidth=lw, marker = marker[i]) for i in range(3)]
lines.append(Line2D([0], [0], color=color_markers_1[0], linewidth=lw, marker = "o"))
lines.append(Line2D([0], [0], color=color_markers_1[1], linewidth=lw, mfc = "None", marker = "o", mew = mew_list[0]))
lines.append(Line2D([0], [0], color=color_markers_1[2], linewidth=lw, mfc = "None", marker = "o", mew = mew_list[1]))

lines.append(Line2D([0], [0], color="black", linewidth=1))

labels = [("$r = %.3f$" % a) for a in [0.01, 0.04, 0.08]]
labels.append("$n = 1$")
labels.append("$n = 3$")
labels.append("$n = 5$")
labels.append("Exact linear")

# Read data from numerical results
for (i,Ni) in enumerate(N):

    print("Computing sliding law for n = %d and r = %.3f" % (ng[i], amp[i]))

    run_dir = "../data/n%d_amp%.3f_N%d/" % (ng[i], amp[i], Ni)

    # Read info
    ep = np.loadtxt(run_dir + "/effpres.txt")
    f = open(run_dir + "/run_info.txt", "r")
    contents = f.read()
    run_info = ast.literal_eval(contents)
    f.close()
    Ny = run_info["Ny"]
    A = run_info["A"]

    # Build sliding law
    mesh = PeriodicRectangleMesh(Ni, Ny, 1, 1, direction = "x")
    V = VectorFunctionSpace(mesh, "CG", 2)
    M = FunctionSpace(mesh, "DG", 0)
    u = Function(V)
    lamda = Function(M)

    def bed_function(x):
        return -amp[i] * (1 - np.sin(0.5*np.pi - 2*np.pi*x))

    _, paths = find_paths(run_dir, Ni)

    print("  %d runs found" % len(paths))

    taubonN = np.zeros(len(paths))
    ubonN = np.zeros(len(paths))
    cpnt = np.zeros(len(paths))
    dpnt = np.zeros(len(paths))

    for (j,path) in enumerate(paths):
        t, ub, c, d = compute_sliding_law_point(run_dir +    path, mesh, u, lamda, bed_function)
        taubonN[j] = t/ep[j]
        ubonN[j] = ub/ep[j]**ng[i]

        if c is not None:
            if c > 0.5:
                c = c - 1
        cpnt[j], dpnt[j] = c, d

    indcavity = np.where(np.isnan(cpnt) == False)[0]

    # Calculate law according to Gudmundsson
    r = amp[i]
    s = (r/taubonN)**ng[i] * ubonN * r * (2*np.pi)**(ng[i] + 2) / (2 * A)
    s = s[0]
    print("  calculated value of c0  :: %.4f" % s)

    x_law = (1./c0[i] * ubonN * r * (2*np.pi)**(ng[i] + 2) / (2 * A))**(1./ng[i])
    y_law = taubonN/r

    # Plot
    if ng[i] == 1:
        fclr = colors[i]
        mew = 1
    elif ng[i] == 3:
        fclr = "None"
        mew = mew_list[0]
    elif ng[i] == 5:
        fclr = "None"
        mew = mew_list[1]

    ax[0].plot(x_law, y_law, mfc = fclr, mec = colors[i], color = colors[i], linewidth = lw, marker = mrk[i], ms = 3, mew = mew)#, label = labels[i])
    ax[1].plot(x_law, cpnt, mfc = fclr, mec = colors[i], color = colors[i], linewidth = lw, marker = mrk[i], ms = 3, mew = mew)#, label = labels[i])
    ax[1].plot(x_law, dpnt, mfc = fclr, mec = colors[i], color = colors[i], linewidth = lw, marker = mrk[i], ms = 3, mew = mew)

# Read Analytical data and plot
if analytical:
    Path = "../data/linearised_results"
    taubonN_a = np.loadtxt(Path + "/taubonN.txt")
    ubonN_a = np.loadtxt(Path + "/ubonN.txt")
    cpnt_a = np.loadtxt(Path + "/cs.txt")
    dpnt_a = np.loadtxt(Path + "/ds.txt")
    indcavity_a = np.where(np.isnan(cpnt_a) == False)[0]
    As_a = ubonN_a[0]/taubonN_a[0]
    C_a = max(taubonN_a)

    cpnt_a = cpnt_a - 0.25
    dpnt_a = dpnt_a - 0.25

    ax[0].plot(ubonN_a*8*np.pi**3, taubonN_a, color = "black", label = "Linear Exact", zorder = 0)
    ax[1].plot(ubonN_a*8*np.pi**3, cpnt_a, color = "black", label = "Linear Exact", zorder = 0)
    ax[1].plot(ubonN_a*8*np.pi**3, dpnt_a, color = "black", zorder = 0)

# Save and close plots
ax[0].legend(lines, labels)
ax[0].set_xlim([0,30])
ax[0].set_ylabel(ylabel_law, rotation=0)
ax[0].set_xlabel(xlabel_law)
ax[0].yaxis.set_label_coords(-0.12,0.5)


# ax_endp.legend()
ax[1].set_xlim([0,30])
ax[1].set_ylabel(ylabel_endp, rotation=0)
ax[1].set_xlabel(xlabel_endp)


fig.savefig("Figure4.pdf", bbox_inches='tight', pad_inches=0.03)
