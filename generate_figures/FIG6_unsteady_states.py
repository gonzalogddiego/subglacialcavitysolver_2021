import numpy as np
from firedrake import *
import matplotlib
from matplotlib import pylab as plt
from figure_settings import pgf_with_latex
import ast
import os

import sys
sys.path.append("../solver/")
from misc import x_sorted_contact_indices, compute_sliding_law_point, find_paths

# Problems under consideration
effpres = [1.88, 1.88]
wl = [2.5, 0.5]
tstart = [5,5]
cycles = [2,2]

amp = 0.08
ng = 3
a = [0.1, 0.1]
N = 192
dt = [0.005, 0.005]
T = [10,10]

show_cavity = True

# Settings
Nplots = len(effpres)
n_rows = 2
if show_cavity:
    n_rows = 3
matplotlib.rcParams.update(pgf_with_latex(n_rows))

# define bed shape
xbed = np.linspace(0,1,N+1)
bed = -amp * (1 - np.sin(0.5*np.pi - 2*np.pi*xbed))
ntheta = 5

###########################
# Obtain data for steady state friction curve
###########################

Nss = 192
run_dir = "../data/n%d_amp%.3f_N%d/" % (ng, amp, Nss)

# Read info
ep_ss = np.loadtxt(run_dir + "/effpres.txt")
f = open(run_dir + "/run_info.txt", "r")
contents = f.read()
run_info = ast.literal_eval(contents)
f.close()
Ny = run_info["Ny"]

# Build sliding law
mesh = PeriodicRectangleMesh(Nss, Ny, 1, 1, direction = "x")
V = VectorFunctionSpace(mesh, "CG", 2)
M = FunctionSpace(mesh, "DG", 0)
u = Function(V)
lamda = Function(M)

_, paths = find_paths(run_dir, Nss)
ub = np.zeros(len(ep_ss))
taub = np.zeros(len(ep_ss))
theta_ss = []
for j, pathj in enumerate(paths):
    theta_vec = np.loadtxt(run_dir + "/" + pathj + "/theta.txt")
    theta_ss.append(theta_vec[-1])
    taub[j], ub[j], _, _ = compute_sliding_law_point(run_dir + pathj, mesh, u, lamda)

data_ss = np.zeros([len(ub), 2])
data_ss[:,0] = ub/ep_ss**ng
data_ss[:,1] = taub/ep_ss


###########################
# Read unsteady state info
###########################
panel_label = []
import string
alphabet = np.array(list(string.ascii_lowercase))
for i in range(Nplots):
    ind_i = np.array(range(n_rows)) + n_rows*i
    panel_label.append(alphabet[ind_i])

colors0 = ["blue", "black", "green"]
fig2, ax2 = plt.subplots(n_rows, Nplots)
plt.subplots_adjust(wspace = 0.35, hspace = 0.6)

for i in range(Nplots):

    path = "../output/unsteady_n%d_N%.2f_Nmesh%d_dt%.5f_wl%.2f_a%.2f" % (ng, effpres[i], N, dt[i], wl[i], a[i])

    ub_us = np.loadtxt(path + "/ub.txt")
    tau_us = np.loadtxt(path + "/tau.txt")
    cv = np.loadtxt(path + "/volume.txt")
    time_steps = int(float(T[i])/dt[i]) + 1
    t = dt[i] * np.array(range(time_steps))
    effpres_us =  np.loadtxt(path + "/effpres.txt")# effpres * (1 + a * np.sin(2 * np.pi / wl * t))

    ind = np.argmin(abs(ep_ss-effpres[i]))


    # Prepare colors
    tplot = np.linspace(tstart[i],tstart[i] + wl[i], int(wl[i]/dt[i]))
    ind_plot = np.array(tplot/dt[i], dtype = "int")
    norm = plt.Normalize()
    colors = plt.cm.plasma(norm(tplot/max(tplot)))

    # first plot
    ax2[0,i].plot(t, ub_us/ub[ind], color = colors0[0])
    ax2[0,i].plot(t, effpres_us/ep_ss[ind], color = colors0[1])

    tmax = 5 + wl[i]*cycles[i]
    ax2[0,i].set_xlim([5,tmax])
    ax2[0,i].set_xlabel("$t$")
    ax2[0,i].set_ylim([0.75,1.6])

    ax22 = ax2[0,i].twinx()
    ax22.plot(t, cv/amp, color = colors0[2])
    ax22.set_ylim([0, 0.35])

    # Set x ticks
    ax2[0,i].xaxis.grid(True)
    ticks_major = np.linspace(5,tmax,int(cycles[i] + 1))
    ax2[0,i].set_xticks(ticks_major, minor=False)
    ticks_minor = np.linspace(5,tmax,int(cycles[i]*4 + 1))
    ax2[0,i].set_xticks(ticks_minor, minor=True)
    ax2[0,i].xaxis.grid(which='minor', linestyle='--')

    # Second plot
    ax2[1,i].set_ylim([0.7,1.6])
    ax2[1,i].set_xlim([0.89,1.11])
    ax2[1,i].set_xlabel('$N/N_0$',rotation=0)
    ax2[1,i].yaxis.set_label_coords(-0.22, 0.465)

    ax2[1,i].plot(ep_ss/ep_ss[ind], ub/ub[ind], color = "black")

    if show_cavity:
        theta_us = np.loadtxt(path + "/theta.txt")
        ind_theta = np.linspace(0,colors.shape[0],ntheta+1)
        ind_theta = np.array(ind_theta[:-1], dtype = "int")
        for j in ind_theta:
            ax2[2,i].plot(xbed, theta_us[ind_plot[j]], color = colors[j])
        ax2[2,i].plot(xbed, bed, color = "black")
        ax2[2,i].plot(xbed, theta_ss[ind], color = "black", linestyle = ":")
        ax2[2,i].set_xlim([0,1])
        ax2[2,i].set_ylim([-2.1 * amp, 0.1*amp])
        ax2[2,i].set_xlabel("$x$")

    # Add color points
    for (j,c) in enumerate(colors):
        ax2[0,i].plot(tplot[j],ub_us[ind_plot[j]]/ub[ind], color = c, marker = "o", markersize = 2)
        ax2[1,i].plot(effpres_us[ind_plot[j]]/ep_ss[ind],ub_us[ind_plot[j]]/ub[ind], color = c, marker = "o", markersize = 3)

    # Remove x axis labels and
    if i == 0:
        ax2[1,i].set_ylabel('$\\frac{u_b}{u_{b,0}}$',rotation=0)

    # set labels
    ax2[0,i].set_title("$f = %.2f$" % (1./wl[i]), fontsize = 8, loc='center')
    for j in range(n_rows):
        ax2[j,i].set_title("(" + panel_label[i][j] + ")", fontsize = 8, loc='left')


# Add legend
from matplotlib.lines import Line2D
lines = [Line2D([0], [0], color=c) for c in colors0]
labels = ['$u_b/u_{b,0}$', "$N/N_{0}$", "$V/(rL^2)$"]
ax2[0,0].legend(lines, labels, bbox_to_anchor=(0.97, 1.15, 0.5, 0.5), ncol = 3, fontsize = 8)
fig2.savefig("unsteady_2.pdf", bbox_inches='tight', pad_inches=0.03)
