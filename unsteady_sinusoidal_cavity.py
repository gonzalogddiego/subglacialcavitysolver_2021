from firedrake import *
import numpy as np
import os
import argparse
import ast
import sys
sys.path.insert(1, "solver")
from sinusoidal_cavity import SinusoidalCavity
from misc import x_sorted_contact_indices, find_paths, CRED, CGREEN, CBLUE, CEND

'''
Compute temporal evolution of cavity under oscillating effective pressure. The
following parameters have to be introduced in terminal ::

N :: number of mesh nodes along x axis
effpres :: mean effective pressure
dt :: time step
amp :: amplitude of bed bump
wl :: wavelength of effective pressure oscillation
a :: non-dimensional amplitude of effective pressure oscillation
T :: time of computation

The following parameters are set to default values but can be changed ::

solver :: velocity-pressure elements, either PkP0 with k = 2 (default) or TH (Taylor Hood)
L :: length of domain
c :: numerical parameter in nonlinear expression for contact conditions
A :: Fluidity parameter
eps :: regularisation of Glen's law
n :: coefficient in Glen's law
'''

parser = argparse.ArgumentParser()

parser.add_argument("--N", type = int, required = True)
parser.add_argument('--effpres', nargs='+', type=float, required = True)
parser.add_argument("--dt", type = float, required = True)
parser.add_argument("--amp", type=float, required = True)
parser.add_argument("--wl", type = float, required = True)
parser.add_argument("--a", type = float, required = True)
parser.add_argument("--T", type = float, required = True)

parser.add_argument("--solver", type=str, default="PkP0", choices=["TH", "PkP0"])
parser.add_argument("--L", type=float, default="1")
parser.add_argument("--c", type = float, default = 1)
parser.add_argument("--A", type=float, default="0.5")
parser.add_argument("--eps", type=float, default="1e-2")
parser.add_argument("--n", type=float, default="1")

args, _ = parser.parse_known_args()

# Initialise solver
Ny = max([int(0.1*args.N), 3])
solver = SinusoidalCavity(args.N, Ny, args.amp, args.L, args.L, False, args.solver)

# Find reference results to setup initial steady state from which to start computations
Nx_ref = args.N
path_ref = "data/n%d_amp%.3f_N%d/" % (args.n, args.amp, Nx_ref)

if os.path.isdir(path_ref) == False:
    print( (CRED + "Data set with N = %d not found" + CEND) % (args.N))
    Nx_ref = 192
    path_ref = "data/n%d_amp%.3f_N%d/" % (args.n, args.amp, Nx_ref)


# Find info on reference
f = open(path_ref + "/run_info.txt", "r")
contents = f.read()
run_info = ast.literal_eval(contents)
f.close()
Ny_ref = run_info["Ny"]
mesh_ref = PeriodicRectangleMesh(Nx_ref, Ny_ref, 1, 1, direction = "x")
A_ref = run_info["A"]
assert A_ref == args.A, "Different fluidity parameter for reference"

ep_steady, paths = find_paths(path_ref, Nx_ref)
ep_steady = np.loadtxt(path_ref + "/effpres.txt")
assert len(ep_steady) != 0, "Steady results do not exist with uniform refinement!"

for ep in args.effpres:

    # Find path with same N and closest ep
    ind = np.argmin(np.abs(round(ep,2) - ep_steady))
    if ep != ep_steady[ind]:
        print( (CRED + "Reference effective pressure %.4f not found; using %.4f instead" + CEND) % (ep, ep_steady[ind]))
        ep = ep_steady[ind]
    path_load = path_ref + paths[ind]

    # Load result
    h5 = HDF5File(path_load + "/solution.xml.gz", "r")

    U = mesh_ref.coordinates.function_space()
    coords = Function(U)
    h5.read(coords, "/coordinates")
    mesh_ref.coordinates.assign(coords)
    indU2Ub, xUb_ref = x_sorted_contact_indices(U, 1)
    theta_ref = mesh_ref.coordinates.dat.data[indU2Ub, 1]


    M = FunctionSpace(mesh_ref, "DG", 0)
    lamda = Function(M)
    h5.read(lamda, "/lamda")
    n = FacetNormal(mesh_ref)
    tau = - assemble(lamda * dot(n, as_vector((1,0))) * ds(1))

    del h5

    # Set mesh according to theta_ref
    theta = np.interp(solver.contact_solver.xUb, xUb_ref, theta_ref)
    nodes_contact_Uc = np.where(theta - solver.bed_points < 0)[0]
    theta[nodes_contact_Uc] = solver.bed_points[nodes_contact_Uc]
    ztop = args.L * np.ones(len(theta))
    solver.deform_mesh.deform(solver.mesh, ztop, theta)

    # Set path to safe
    path_out = "output/unsteady_n%d_N%.2f_Nmesh%d_dt%.5f_wl%.2f_a%.2f" % (args.n, ep, args.N, args.dt, args.wl, args.a)

    iter = 0
    path_new = path_out
    while os.path.exists(path_new):
        path_new = path_out + ("_alt%d" % iter)
        iter += 1

    if iter > 0:
        path_out = path_new
    os.mkdir(path_out)

    # Run
    time_steps = int(float(args.T)/args.dt) + 1
    t = args.dt * np.array(range(time_steps))
    ep_unsteady = ep * (1 + args.a * np.sin(2 * np.pi / args.wl * t))
    solver.solve(ep_unsteady,args.dt, args.c, args.A, args.eps, args.n, "unsteady", T_unsteady = args.T, tau = tau)

    # Save files
    solver.save_files(path_out)
    np.savetxt(path_out + "/effpres.txt", np.array(ep_unsteady))

    # Plot
    from matplotlib import pylab as plt

    fig1, ax1 = plt.subplots()
    fig2, ax2 = plt.subplots()
    fig3, ax3 = plt.subplots()

    ax1.plot(t, solver.taub)
    fig1.savefig(path_out + "/tau.png")

    ax2.plot(t, solver.cavity_volume)
    fig2.savefig(path_out + "/cavity_volume.png")

    ax3.plot(t, solver.ub)
    fig3.savefig(path_out + "/ub.png")

    plt.close(fig1)
    plt.close(fig2)
    plt.close(fig3)
