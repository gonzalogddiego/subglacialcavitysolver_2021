from firedrake import *
import numpy as np
import os
import argparse
from datetime import datetime
import sys
sys.path.insert(1, "solver")
from sinusoidal_cavity import SinusoidalCavity
from misc import CBLUE, CEND

'''
Generate a sequence of steady states in order to reconstruct a sliding law. The
following parameters have to be introduced in terminal ::

N :: number of mesh nodes along x axis
amp :: amplitude of bed bump
n :: coefficient in Glen's law
epmin :: smallest effective pressure to compute
epmax :: largest effective pressure to compute
Nep :: number of points along sliding law to compute between epmin and epmax

solver :: velocity-pressure elements, either PkP0 with k = 2 (default) or TH (Taylor Hood)
CFL :: sets time step dt via dt = CFL/N
kmax :: limit number of iterations before ending computation to steady state
tol :: threshold value of (theta^{k+1} - theta^k)/dt to determine whether steady
       state has been reached
c :: numerical parameter in nonlinear expression for contact conditions
eps :: regularisation of Glen's law

'''

date = datetime.today().strftime('%Y%m%d%H%M%S')

parser = argparse.ArgumentParser()
parser.add_argument("--N", type = int, required = True)
parser.add_argument("--amp", type = float, required = True)
parser.add_argument("--n", type = int, required = True)
parser.add_argument("--epmin", type = float, required = True)
parser.add_argument("--epmax", type = float, required = True)
parser.add_argument("--Nep", type = int, required = True)

parser.add_argument("--solver", type=str, default="PkP0", choices=["TH", "PkP0"])
parser.add_argument("--CFL", type = float, default = 0.96)
parser.add_argument("--kmax", type=int, default=1e4)
parser.add_argument("--tol", type=float, default="1e-3")
parser.add_argument("--c", type = float, default = 1)
parser.add_argument("--eps", type=float, default="1e-2")
args, _ = parser.parse_known_args()

# Set further values for computations
L = 1
H = 1
utop = 1
A = 0.5
dt = args.CFL/args.N
Ny = max([int(0.1*args.N), 3])

# The points along the sliding law to compute are given by equally spaced values of
# effective pressure between epmax and epmin
effpres = np.linspace(args.epmax, args.epmin, args.Nep)


# Initialise solver
solver = SinusoidalCavity(args.N, Ny, args.amp, L, H, False, args.solver)
zold = Function(solver.Z)

# Create folder in which to save results
path_folder = "data/n%d_amp%.3f_N%d" % (args.n,args.amp,args.N)

iter = 0
path_new = path_folder
while os.path.exists(path_new):
    path_new = path_folder + ("_alt%d" % iter)
    iter += 1

if iter > 0:
    path_folder = path_new

os.mkdir(path_folder)

# Save information of run in txt file
run_info = {
    "mesh type" : "PeriodicRectangleMesh",
    "quad" : False,
    "Ny" : Ny,
    "L" : L,
    "H" : H,
    "utop" : utop,
    "A" : A,
    "eps" : args.eps,
    "dt" : dt,
    "c" : args.c,
    "tol" : args.tol,
    "kmax" : args.kmax
}
f = open(path_folder + "/run_info.txt","w")
f.write( str(run_info) )
f.close()
np.savetxt(path_folder + "/effpres.txt", np.array(effpres))

# Run cases
tau_b = np.zeros(len(effpres))
u_b = np.zeros(len(effpres))

for i,ep in enumerate(effpres):
    print((CBLUE + "Effpres = %.2f" + CEND) % ep)
    solver.solve(ep, dt, args.c, A, args.eps, args.n, "steady",\
        tol_steady = args.tol, kmax_steady = args.kmax, utop = utop, zold = zold)
    zold.assign(solver.z)
    path = path_folder + "/Nmesh%d_n%d_amp%.2f_N%.2f_u%.2f_dt%.4f" % (args.N, args.n, args.amp, ep, utop, dt)
    os.mkdir(path)
    solver.save_all(path)
    tau_b[i] = solver.taub[-1]
    u_b[i] = solver.ub[-1]

# Plot sliding law
c0 = (args.amp/tau_b[0])**args.n * u_b[0] * args.amp * (2*np.pi)**(args.n + 2) / (2 * A)
print("c0 = %.4f" % c0)
alpha = 1./c0 * 0.5 * (2 * np.pi)**(args.n + 2)

import matplotlib.pylab as plt
effpres = np.array(effpres)
y = tau_b / effpres/args.amp
x = (alpha * u_b * args.amp / A/L)**(1./args.n) / effpres
fig, ax = plt.subplots()
ax.plot(x, y, marker = "o", markersize = 4, linewidth = 1)
fig.savefig(path_folder + "/friction.png")
