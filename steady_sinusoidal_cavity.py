import argparse
from firedrake import *
import numpy as np
import os
import sys
sys.path.insert(1, "solver")
from sinusoidal_cavity import SinusoidalCavity

'''
Find steady states of subglacial cavities. The following parameters have to be introduced
in terminal ::

N :: number of mesh nodes along x axis
effpres :: effective pressure
dt :: time step
amp :: amplitude of bed bump

The following parameters are set to default values but can be changed ::

solver :: velocity-pressure elements, either PkP0 with k = 2 (default) or TH (Taylor Hood)
kmax :: limit number of iterations before ending computation to steady state
tol :: threshold value of (theta^{k+1} - theta^k)/dt to determine whether steady
       state has been reached
utop :: horizontal velocity at top boundary
L :: length of domain
c :: numerical parameter in nonlinear expression for contact conditions
A :: Fluidity parameter
eps :: regularisation of Glen's law
n :: coefficient in Glen's law

'''

parser = argparse.ArgumentParser()
parser.add_argument("--N", type = int, required = True)
parser.add_argument('--effpres', type=float, required = True)
parser.add_argument("--dt", type = float, required = True)
parser.add_argument("--amp", type=float, required = True)

parser.add_argument("--solver", type=str, default="PkP0", choices=["TH", "PkP0"])
parser.add_argument("--kmax", type=int, default=1e4)
parser.add_argument("--tol", type=float, default="1e-4")
parser.add_argument("--utop", type=float, default="1")
parser.add_argument("--L", type=float, default="1")
parser.add_argument("--c", type = float, default = 1)
parser.add_argument("--A", type=float, default="0.5")
parser.add_argument("--eps", type=float, default="1e-2")
parser.add_argument("--n", type=float, default="1")
args, _ = parser.parse_known_args()

Ny = max([int(0.1*args.N), 3]) # elements in the y direction

# Initialise solver and run
solver = SinusoidalCavity(args.N, Ny, args.amp, args.L, args.L, False, args.solver)
solver.solve(args.effpres, args.dt, args.c, args.A, args.eps, args.n,\
    "steady", tol_steady = args.tol, kmax_steady = args.kmax, utop = args.utop)

# Save info
path_out = "output/Nmesh%d_n%d_amp%.2f_N%.2f_dt%.4f" % (args.N, args.n, args.amp, args.effpres, args.dt)

iter = 0
path_new = path_out
while os.path.exists(path_new):
    path_new = path_out + ("_alt%d" % iter)
    iter += 1

if iter > 0:
    path_out = path_new

os.mkdir(path_out)
solver.save_all(path_out)
