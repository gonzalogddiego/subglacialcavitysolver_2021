# Finite Element Solver for Subglacial Cavities

This repository contains the codes used to produce the results presented in the article "Numerical approximation of viscous contact problems applied to glacial sliding" by de Diego, G.G., Farrell, P.E. and Hewitt, I.J. This finite element solver is implemented in Firedrake using the version available [in this repository](https://zenodo.org/record/5643646#.YYQeFb9_oYs) with doi `10.5281/zenodo.5643646`.

# Running the implementation

**Steady states**. The steady state of a cavity can be computed by running 

`python steady_sinusoidal_cavity.py --N N --dt dt --amp amp --effpres ep --n n`

where `N` is the number of elements along the lower boundary, `dt` the time step, `amp` the amplitude of the sinusoidal bedrock, `ep` the effective pressure and `n` the exponent in Glen's law.

**Reconstruct steady sliding law**. A sliding law can be reconstructed by computing a sequence of steady states each defined by the horizontal velocity `utop` and the effective pressure `effpres`. In this implementation, we fix `utop = 1` and vary `effpres`. The sliding law is computed by running 

`python build_sliding_law.py --N N --amp amp --n n --epmin epmin --epmax epmax --Nep Nep`

where `N`, `amp` and `n` are as above. The numbers `epmin`, `epmax` and `Nep` set the values of `effpres` to be used when reconstructing the sliding law. These values are given by `Nep` equally spaced numbers between `epmin` and `epmax`. In these computations, a default timestep is set proportional to the horizontal mesh size. 

**Unsteady computations**. Once we have the data for a sliding law, we can compute the temporal evolution of a cavity from a steady state under oscillating effective pressures. By default, the code will seek a database of sliding law points computed for `Nref = 192` for a given `amp` and `n`. For this reason, `build_sliding_law.py` must have been run previously. The unsteady computations are started by running 

`python unsteady_sinusoidal_cavity.py --N N --dt dt --amp amp --effpres ep --n n --T T --wl wl --a a`

where `N`, `dt` and `n` are as above. The value `ep` sets the steady state from which the system is evolved, with `utop = 1` by default. The time dependent effective pressure is given by a sinusoidal wave of amplitude `a * ep` and wavelength `wl`. The computations are run until the time `T` is reached. 

# Generating Figure 3 and Table 1

To generate Figure 3 of the paper, we first need to run the different test cases for the 5 values of N presented in the paper:

`python steady_sinusoidal_cavity.py --n 1 --amp 0.01 --effpres 0.3 --N 16 --dt 0.05`

`python steady_sinusoidal_cavity.py --n 1 --amp 0.01 --effpres 0.3 --N 32 --dt 0.025`

`python steady_sinusoidal_cavity.py --n 1 --amp 0.01 --effpres 0.3 --N 64 --dt 0.01`

`python steady_sinusoidal_cavity.py --n 1 --amp 0.01 --effpres 0.3 --N 128 --dt 0.005`

`python steady_sinusoidal_cavity.py --n 1 --amp 0.01 --effpres 0.3 --N 192 --dt 0.005`

Once these results have been saved in the output folder, we can generate the figures by running, in the directory `generate_figures`, the command

`python FIG3_compare_steady_states.py`

# Generating Figure 4

We first generate the data for the 9 sliding laws as follows

`python build_sliding_law.py --N 192 --amp 0.01 --n 1 --epmin 0.1 --epmax 2.6 --Nep 100`

`python build_sliding_law.py --N 192 --amp 0.04 --n 1 --epmin 0.25 --epmax 2.75 --Nep 100`

`python build_sliding_law.py --N 192 --amp 0.08 --n 1 --epmin 0.5 --epmax 5.5 --Nep 100`

`python build_sliding_law.py --N 192 --amp 0.01 --n 3 --epmin 0.3 --epmax 4.3 --Nep 100`

`python build_sliding_law.py --N 192 --amp 0.04 --n 3 --epmin 0.5 --epmax 4.5 --Nep 100`

`python build_sliding_law.py --N 192 --amp 0.08 --n 3 --epmin 0.75 --epmax 3.5 --Nep 100`

`python build_sliding_law.py --N 192 --amp 0.01 --n 5 --epmin 0.5 --epmax 4.5 --Nep 100`

`python build_sliding_law.py --N 192 --amp 0.04 --n 5 --epmin 0.7 --epmax 4.7 --Nep 100`

`python build_sliding_law.py --N 192 --amp 0.08 --n 5 --epmin 1 --epmax 5 --Nep 100`

Once the results have been saved in the data folder, we can generate the figures by running, in the directory `generate_figures`, the command

`python FIG4_compare_sliding_curves.py`

# Generating Figures 5, 6 and 7

We first generate the data by running

`python unsteady_sinusoidal_cavity.py --N 192 --effpres 2.23 --amp 0.08 --n 3 --dt 0.005  --a 0.1 --wl 2.5 --T 10`

`python unsteady_sinusoidal_cavity.py --N 192 --effpres 1.88 --amp 0.08 --n 3 --dt 0.005  --a 0.1 --wl 2.5 --T 10`

`python unsteady_sinusoidal_cavity.py --N 192 --effpres 1.88 --amp 0.08 --n 3 --dt 0.005  --a 0.1 --wl 0.5 --T 10`

`python unsteady_sinusoidal_cavity.py --N 192 --effpres 1.09 --amp 0.08 --n 3 --dt 0.005  --a 0.1 --wl 2.5 --T 10`

`python unsteady_sinusoidal_cavity.py --N 192 --effpres 1.09 --amp 0.08 --n 3 --dt 0.005  --a 0.01 --wl 2.5 --T 10`

To generate figures 5, 6 and 7 of the paper we run, in the directory `generate_figures`, the command

`python FIG5_unsteady_states.py`

`python FIG6_unsteady_states.py`

`python FIG7_unsteady_states.py`

We note that data from the sliding curve for n = 3 and amp = 0.08 must have been generated before for the figures to be generated

