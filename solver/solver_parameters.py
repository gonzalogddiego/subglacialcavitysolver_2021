from firedrake import *
import numpy as np

def parameters_multiplier(contact_subdomain):
    params_nl = {
             "snes_monitor": None,
             "snes_linesearch_type": "l2",
             "snes_max_it": 1000,
             "snes_rtol": 1.0e-8,
             "snes_atol": 1.0e-8,
             "snes_stol": 0,
             "snes_converged_reason": None,
             "snes_linesearch_monitor": None,
             }

    params_lu = {
             "ksp_type": "preonly",
             "ksp_monitor": None,
             "pc_type" : "lu",
             "mat_type" : "aij",
             "pc_factor_mat_solver_type": "mumps",
             "mat_mumps_icntl_14": 200,
             }

    params_fs = {
        "ksp_type": "fgmres",
        "ksp_max_it": 1,
        "mat_type": "aij",
        "pc_type": "python",
        "pc_python_type": __name__ + ".LagrangeMultiplierPC",
        "lm_pc_type": "fieldsplit",
        "lm_pc_fieldsplit_type": "additive",
        "lm_boundary_labels": contact_subdomain,
        "lm_fieldsplit_0": params_lu,
        "lm_fieldsplit_1_ksp_type": "preonly",
        "lm_fieldsplit_1_pc_type": "none",
    }

    return {**params_nl, **params_fs}

class LagrangeMultiplierPC(PCBase):
    def initialize(self, pc):

        from firedrake.petsc import PETSc
        from firedrake.dmhooks import get_function_space
        from numpy import concatenate

        self.pc = PETSc.PC().create(comm=pc.comm)
        A, P = pc.getOperators()
        self.pc.setOperators(A, P)
        self.pc.setType("fieldsplit")

        prefix = pc.getOptionsPrefix()
        options_prefix = prefix + "lm_"
        self.pc.setOptionsPrefix(options_prefix)

        dm = pc.getDM()
        Z = get_function_space(dm)
        assert len(Z._ises) == 3

        # get the boundary labels to keep
        boundary_labels_to_keep = PETSc.Options(options_prefix).getString("boundary_labels", default=None)
        if boundary_labels_to_keep is None:
            raise ValueError("Need to specify which boundary labels should be retained")
        boundary_labels_to_keep = map(int, boundary_labels_to_keep.split(","))

        local_nodes_on_boundary = Z.sub(2).boundary_nodes(boundary_labels_to_keep, "geometric")
        local_all_nodes = Z.sub(2).dof_dset.local_ises[0].indices[:Z.sub(2).dof_dset.size]
        indices_to_lose = np.array([i for (i, x) in enumerate(local_all_nodes) if x not in local_nodes_on_boundary])
        indices_to_keep = np.array([i for (i, x) in enumerate(local_all_nodes) if x     in local_nodes_on_boundary])

        global_lm_to_keep = Z.dof_dset.lgmap.apply(Z.dof_dset.local_ises[2].indices[indices_to_keep])
        global_dofs_is0 = concatenate((Z._ises[0].indices, Z._ises[1].indices, global_lm_to_keep))
        global_dofs_is1 = Z.dof_dset.lgmap.apply(Z.dof_dset.local_ises[2].indices[indices_to_lose])

        is0 = PETSc.IS().createGeneral(sorted(global_dofs_is0), comm=pc.comm)
        is1 = PETSc.IS().createGeneral(sorted(global_dofs_is1), comm=pc.comm)

        self.pc.setFieldSplitIS(('0', is0), ('1', is1))
        self.pc.setFromOptions()
        self.pc.setUp()

    def update(self, pc):
        pass

    def apply(self, pc, X, Y):
        self.pc.apply(X, Y)

    def applyTranspose(self, pc, X, Y):
        self.pc.applyTranspose(X, Y)
