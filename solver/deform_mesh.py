from firedrake import *
from misc import x_sorted_contact_indices
import numpy as np

class Deform2DExtrudedMesh(object):

    def __init__(self, mesh, contact_subdomain):

        # Define y_original
        coords = mesh.coordinates.dat.data
        self.y_original = np.zeros(coords.shape[0])
        self.y_original[:] = coords[:,1]

        # Establish connectivity_array
        _, xUb = x_sorted_contact_indices(mesh.coordinates.function_space(), contact_subdomain)
        self.connectivity_array = []
        total_ind = 0

        for (i,xi) in enumerate(xUb):
            ind_i = np.where(coords[:,0] == xi)[0]
            self.connectivity_array.append(ind_i)
            total_ind += ind_i.size

        assert total_ind == coords[:,0].size, "not every ind covered"

    def deform(self, mesh, ytop, ybed):

        assert len(self.connectivity_array) == len(ytop), "Incoherent size of ytop"
        assert len(self.connectivity_array) == len(ybed), "Incoherent size of ybed"
        assert min(ytop - ybed) > 0, "ytop not strictly larger than ybed"
        mesh_coords = mesh.coordinates.dat.data
        delta_new = ytop - ybed

        for (i,ind_i) in enumerate(self.connectivity_array):
            ybed_original_i = min(self.y_original[ind_i])
            delta_old_i = max(self.y_original[ind_i]) - min(self.y_original[ind_i])
            mesh_coords[ind_i,1] = ybed[i] + delta_new[i]/delta_old_i * (self.y_original[ind_i] - ybed_original_i)
