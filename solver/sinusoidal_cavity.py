from firedrake import *
import numpy as np
from subglacial_cavity_solver import SubglacialCavitySolver

class SinusoidalCavity(SubglacialCavitySolver):

    def Mesh(self, Nx, Ny, L, H, quad):

        alpha = 2
        mesh = PeriodicRectangleMesh(Nx, Ny, L, H, direction = "x", quadrilateral = quad)
        coords = mesh.coordinates.dat.data
        coords[:,1] = (np.exp(2*coords[:,1]/H)-1)/(np.exp(2) - 1)*H

        contact_subdomain = 1
        top_subdomain = 2

        return mesh, contact_subdomain, top_subdomain

    def bedrock(self, x, amp, L):
        return -amp*(1-np.sin(0.5*np.pi - 2*np.pi*x/L))
