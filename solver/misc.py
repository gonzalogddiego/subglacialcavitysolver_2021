from firedrake import *
import numpy as np
import os
import re

CRED = '\033[101m'
CGREEN = '\033[102m'
CBLUE = '\33[104m'
CEND = '\033[0m'

class DirichletBCDoFs(DirichletBC):
    def __init__(self,indices_dofs, V, g, method = "geometric"):
        super().__init__(V, g, None)
        self.indices_dofs = np.array(indices_dofs, dtype = "int32")
    @utils.cached_property
    def nodes(self):
        return self.indices_dofs

def calculate_mean(u, M, indMb2M, contact_subdomain):
    un = np.zeros(len(indMb2M))
    for i, ind in enumerate(indMb2M):
        indicator = Function(M)
        indicator.dat.data[ind] = 1
        un[i] = assemble(indicator * u * ds(contact_subdomain))/assemble(indicator * ds(contact_subdomain))
    return un

def quadrature_linear(x,y):
    return (0.5 * (y[1:] + y[:-1]) * (x[1:] - x[:-1])).sum()

def l2_norm_linear(x,y):
    n_gl = 4
    pnts_gl, w_gl = np.polynomial.legendre.leggauss(n_gl)
    w_gl = 0.5*w_gl
    h = x[1:] - x[:-1]
    m = 0.5 * (x[1:] + x[:-1])
    integral = 0

    for i, hi in enumerate(h):
        xpnts = m[i] + 0.5 * hi * pnts_gl
        ypnts = np.interp(xpnts, x, y)**2
        integral += hi * (w_gl * ypnts).sum()

    return integral**0.5

def plus(x):
    return conditional(ge(x, 0), x, 0)

def glens_law(u, eps_min_val, A_val, ng_val):
    """Strain-rate dependent viscosity:
       0.5*A(T)^{-1/ng}*eps_2*((1-ng)/(2*ng))"""

    strain = sym(grad(u))
    eps_2 = 0.5 * tr(dot(strain, strain)) + eps_min_val**2.0
    #eps_2 = eps_min_val**2.0
    visc = 0.5 * A_val**(-1.0/ng_val) * eps_2**((1.0-ng_val)/(2.0*ng_val))
    return visc

def x_sorted_contact_indices(U, contact_subdomain):
    dofU_contact = U.boundary_nodes(contact_subdomain, method = "geometric")
    mesh = U.mesh()
    U_ufl = U.ufl_element()
    xyU = interpolate(SpatialCoordinate(mesh), VectorFunctionSpace(mesh, U_ufl.family(), U_ufl.degree()))
    xU_contact = xyU.dat.data[dofU_contact, 0]

    # Avoid repeated values in x (for periodic meshes!)
    seen = set()
    xnew = []
    indnew = []
    for i,e in enumerate(xU_contact):
        if e not in seen:
            indnew.append(i)
            xnew.append(e)
            seen.add(e)

    xU_contact = np.array(xnew)
    dofU_contact = dofU_contact[indnew]
    ind_sort = np.argsort(xU_contact)

    return dofU_contact[ind_sort], xU_contact[ind_sort]

def find_paths(run_path, N):
    runs = os.listdir(run_path)
    paths = []
    effpres = []

    for run in runs:

        ints = re.findall(r'\d+', run)

        if len(ints) == 0:
            continue

        if os.path.isdir(run_path + run) is False:
            continue

        if int(ints[0]) == N:
            ep = float(re.findall('\d+\.\d+', run)[1])
            paths.append(run)
            effpres.append(ep)

    effpres = np.array(effpres)
    paths = np.array(paths)
    ind_sort = np.argsort(effpres)
    effpres = effpres[ind_sort[::-1]]
    paths = paths[ind_sort[::-1]]

    return effpres, paths


def calculate_contact_points(x, theta, bed):

    i_contact = np.where( np.abs(theta - bed) < 1e-9)[0]
    i_free = np.array(list(set(range(len(bed))) - set(i_contact)))

    assert i_contact.size !=0, "no contact points"


    if i_contact.size == x.size:
        cpnt = None
        dpnt = None
    else:
        if 0 in i_contact:
            cpnt = x[i_free[0] - 1]
            dpnt = x[(i_free[-1] + 1)]# % len(bed)]
        else:
            dpnt = x[i_contact[0]]
            cpnt = x[i_contact[-1]]

        if cpnt == 0:
            cpnt = x[-1]

    return cpnt, dpnt

def compute_sliding_law_point(path, mesh, u, lamda, bed_function = None):

    # Read solutions and deform mesh
    coords = Function(mesh.coordinates.function_space())
    h5 = HDF5File(path + "/solution.xml.gz", "r")
    h5.read(u, "/velocity")
    h5.read(lamda, "/lamda")
    h5.read(coords, "/coordinates")
    del h5
    mesh.coordinates.dat.data[:] = coords.dat.data

    # Calculate tau_b and ub
    n = FacetNormal(mesh)
    taub = - assemble(lamda * dot(n, as_vector((1,0))) * ds(1))
    ub = assemble(dot(u, as_vector((1,0))) * ds(1))

    # Calculate contact points
    if bed_function == None:
        cpnt = None
        dpnt = None
    else:
        indU2Ub, x = x_sorted_contact_indices(mesh.coordinates.function_space(), 1)
        theta = mesh.coordinates.dat.data[indU2Ub,1]
        bed = bed_function(x)
        cpnt, dpnt = calculate_contact_points(x, theta, bed)

    return taub, ub, cpnt, dpnt
