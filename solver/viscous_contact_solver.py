from firedrake import *
import numpy as np
from misc import plus, glens_law, calculate_mean, x_sorted_contact_indices, DirichletBCDoFs
from solver_parameters import parameters_multiplier
from deform_mesh import Deform2DExtrudedMesh

class ViscousContactSolver(object):

    def __init__(self, mesh, k, M, contact_subdomain):

        self.contact_subdomain = contact_subdomain

        # Define function space
        self.Z = self.function_space(mesh, k, M)

        # Find indices associated to boundary in M
        self.nodes_on_boundary = M.boundary_nodes(contact_subdomain, "geometric")
        self.nodes_function_space = list(range(M.dim()))

        # This is very problem dependent: 2D and left to right. More general?
        self.indMb2M, self.xMb = x_sorted_contact_indices(M, contact_subdomain)
        self.indU2Ub, self.xUb = x_sorted_contact_indices(mesh.coordinates.function_space(), contact_subdomain)

        # Initialise mesh deformation module
        self.deform_mesh = Deform2DExtrudedMesh(mesh, contact_subdomain)

    # The functions below introduce the necessary ingredients for solving the advection equation
    def solve_advection(self, z, F, bcs, dt, theta_, bed, tol):

        # Solve Stokes at t_k
        z = self.solve_stokes(z, F, bcs, theta_, bed, tol)
        u, p, lamda = z.split()

        # Advance free surface
        M = z.ufl_function_space().sub(2)
        theta = self.advance_theta(theta_, u, dt, M)

        # Correct points in contact in lower boundary
        nodes_contact_Uc = np.where(theta - bed < tol)[0]
        theta[nodes_contact_Uc] = bed[nodes_contact_Uc]

        return u, p, lamda, theta

    def advance_theta(self, theta_, u, dt, M):

        # Define advection term at t_k
        mesh = u.ufl_domain()
        un = calculate_mean(dot(u,FacetNormal(mesh)), M, self.indMb2M, self.contact_subdomain)
        dtheta_dx = (theta_[1:] - theta_[:-1])/(self.xUb[1:] - self.xUb[:-1])
        advection_term = un * np.sqrt(dtheta_dx**2 + 1)

        # Advance theta to t_k+1
        theta = np.zeros(len(theta_))
        theta[1:] = theta_[1:] - dt * advection_term

        # Periodicity
        theta[0] = theta[:1]


        return theta

    def solve_stokes(self, z, F, bcs, theta, bed, tol):

        mesh = z.ufl_domain()
        Z = z.ufl_function_space()

        # Set nodes and deform
        nodes_complement, _, _ = self.set_nodes(theta, bed, tol)
        ztop = max(mesh.coordinates.dat.data[:,1]) * np.ones(len(theta))
        self.deform_mesh.deform(mesh, ztop, theta)

        # Define boundary conditions
        bcs_new = []
        if not isinstance(bcs, list):
            bcs_new = [bcs, DirichletBCDoFs(nodes_complement, Z.sub(2), Constant(0))]
        else:
            bcs_new.extend(bcs)
            bcs_new.append(DirichletBCDoFs(nodes_complement, Z.sub(2), Constant(0)))

        # Obtain solver parameters
        parameters = parameters_multiplier(self.contact_subdomain)

        # Solve
        stokes_solver = self.stokes_solver(F, z, bcs_new, parameters)
        stokes_solver.solve()

        return z

    def set_nodes(self, theta, bed, tol):

        dtheta = theta[1:] - bed[1:]
        ind_free_boundary_sorted = np.where((dtheta > tol))[0]
        self.ind_free_boundary_sorted = ind_free_boundary_sorted

        nodes_free_boundary = self.indMb2M[ind_free_boundary_sorted]
        nodes_contact = np.array(list(set(self.nodes_on_boundary) - set(nodes_free_boundary)), dtype="int32")
        nodes_complement = np.array(list(set(self.nodes_function_space) - set(nodes_contact)), dtype="int32")

        return nodes_complement, nodes_contact, nodes_free_boundary

    # These functions introduce everything required for the Stokes solver
    def function_space(self, k, M):
        raise NotImplementedError

    def a(self, u, v, ng_val, A_val, eps):
        raise NotImplementedError

    def residual(self, z, ng_val, A_val, eps, c, contact_subdomain):
        mesh = z.ufl_domain()
        Z = z.function_space()
        n = FacetNormal(mesh)
        (u, p, lamda) = split(z)
        (v, q, mu) = split(TestFunction(Z))
        n = FacetNormal(mesh)
        complementarity = plus( - lamda + c * facet_avg(dot(u,n)))

        F = (
            self.a(u, v, ng_val, A_val, eps)
            - p * div(v) * dx
            - div(u) * q * dx
            - lamda * dot(v, n) * ds(contact_subdomain)
            + lamda * mu * ds(contact_subdomain)
            + complementarity * mu * ds(contact_subdomain)
        )

        return F

    def stokes_solver(self, F, z, bcs, solver_paramaters):
        problem = NonlinearVariationalProblem(F, z, bcs = bcs)
        solver = NonlinearVariationalSolver(problem, solver_parameters = solver_paramaters)
        return solver


class Pkp0Solver(ViscousContactSolver):

    def function_space(self, mesh, k, M):
        tdim = mesh.topological_dimension()
        if k < tdim:
            Pk = FiniteElement("Lagrange", mesh.ufl_cell(), k)
            FB = FiniteElement("FacetBubble", mesh.ufl_cell(), tdim)
            eleu = VectorElement(NodalEnrichedElement(Pk, FB))
        else:
            Pk = FiniteElement("Lagrange", mesh.ufl_cell(), k)
            eleu = VectorElement(Pk)
        elep = FiniteElement("Discontinuous Lagrange", mesh.ufl_cell(), 0)
        V = FunctionSpace(mesh, eleu)
        Q = FunctionSpace(mesh, elep)
        return MixedFunctionSpace([V, Q, M])

    def a(self, u, v, ng_val, A_val, eps):
        epsilon = sym(grad(u))
        eta = glens_law(u, eps, A_val, ng_val)
        tau = 2*eta*epsilon
        return inner(tau, grad(v))*dx

class TaylorHoodSolver(Pkp0Solver):

    def function_space(self, mesh, k, M):
        assert k == 2, "not sure if TH works with k != 2"
        V = VectorFunctionSpace(mesh, "CG", k)
        Q = FunctionSpace(mesh, "CG", k-1)
        return MixedFunctionSpace([V, Q, M])
