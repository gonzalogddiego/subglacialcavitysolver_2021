from firedrake import *
import numpy as np
from viscous_contact_solver import Pkp0Solver, TaylorHoodSolver
from misc import quadrature_linear, l2_norm_linear, calculate_mean, CRED, CGREEN, CBLUE, CEND
from matplotlib import pylab as plt


class SubglacialCavitySolver(object):

    def __init__(self, Nx, Ny, amp, L, H, quad, solver):

        # Define mesh
        mesh, contact_subdomain, top_subdomain = self.Mesh(Nx, Ny, L, H, quad)
        self.mesh = mesh
        self.contact_subdomain = contact_subdomain
        self.top_subdomain = top_subdomain

        # Define function space properties
        k = 2
        M = FunctionSpace(mesh, "DG", 0)

        # Initialise ViscousContactSolver
        assert solver in ("PkP0", "TH"), "Invalid solver :: " + solver
        if solver == "PkP0":
            self.contact_solver = Pkp0Solver(mesh, k, M, contact_subdomain)
        elif solver == "TH":
            self.contact_solver = TaylorHoodSolver(mesh, k, M, contact_subdomain)

        self.Z = self.contact_solver.Z
        self.deform_mesh = self.contact_solver.deform_mesh

        # Set initial condition
        self.bed_points = self.bedrock(self.contact_solver.xUb, amp, L)
        self.deform_mesh.deform(mesh, H * np.ones(len(self.bed_points)), self.bed_points)

    def Mesh(self,Nx, Ny, L, H, quad):
        NotImplementedError

    def bedrock(self, x, amp, L):
        NotImplementedError

    def solve(self, effpres, dt, c, A_val, eps, ng, state,
                tol_steady = None, kmax_steady = None, utop = None,
                T_unsteady = None, tau = None,
                zold = None):

        assert state in {"steady", "unsteady",}, "Invalid state type %s" % state

        tol_contact = 1e-9

        # Set theta
        theta_ = self.mesh.coordinates.dat.data[self.contact_solver.indU2Ub, 1]

        # Set functions and residual
        z = Function(self.Z)
        F = self.contact_solver.residual(z, ng, A_val, eps, c, self.contact_subdomain)
        n = FacetNormal(self.mesh)
        v, q, mu = split(TestFunction(self.Z))

        if zold is not None:
            z.assign(zold)

        # Set differences depending on state
        if state == "steady":
            assert None not in (tol_steady, kmax_steady, utop), "Introduce tol_steady and kmax_steady for steady computations"
            termination_criterion = self.steady_termination_criterion
            kmax = int(kmax_steady)
            effpres = effpres * np.ones(kmax)
            bc = DirichletBC(self.Z.sub(0).sub(0), Constant(utop), self.top_subdomain)

        elif state == "unsteady":
            assert None not in (T_unsteady, tau), "Introduce T_unsteady for unsteady computations"
            termination_criterion = self.unsteady_termination_criterion
            kmax = int(float(T_unsteady)/dt) + 1
            F += - Constant(tau) * v[0] * ds(self.top_subdomain)
            bc = []

        # Initialise loop
        cavity_volume = []
        norm_unsteady = []
        taub = []
        ub = []
        theta_dofs = []
        lamda_dofs = []
        un_dofs = []

        k = 0

        while True:

            print((CGREEN + "Iteration = %d" + CEND) % float(k))
            print((CGREEN + "t = %.5f" + CEND) % float(dt*k))
            print((CBLUE + "ep = %.5f" + CEND) % effpres[k])

            # Solve advection equation
            F_new = F + effpres[k] * dot(v, n) * ds(self.top_subdomain)
            u, p, lamda, theta = self.contact_solver.solve_advection(z, F_new, bc, dt, theta_, self.bed_points, tol_contact)
            theta[0] = theta[-1]

            # Calculations
            cavity_volume.append(quadrature_linear(self.contact_solver.xUb, theta_ - self.bed_points))
            taub.append(- assemble(lamda * dot(n, as_vector((1,0))) * ds(self.contact_subdomain)))
            ub.append(assemble(u[0] * ds(self.contact_subdomain)))
            theta_dofs.append(theta_)
            lamda_dofs.append(lamda.dat.data[self.contact_solver.indMb2M])
            norm_unsteady.append(1./dt * l2_norm_linear(self.contact_solver.xUb, theta - theta_))
            un_dofs.append(calculate_mean(dot(u,FacetNormal(self.mesh)), self.Z.sub(2), self.contact_solver.indMb2M, self.contact_subdomain))

            # Update theta_
            theta_ = theta

            # Check criterion
            print( (CBLUE + "int sigma_nn * nx = %.4e" + CEND) % taub[k])
            print( (CBLUE + "L2 norm unsteady = %.4e" + CEND + "\n") % norm_unsteady[k])

            k += 1
            if termination_criterion(k, norm_unsteady[k-1], tol_steady, kmax):
                break

        self.z = z
        self.k = k
        self.dt = dt
        self.cavity_volume = cavity_volume
        self.norm_unsteady = norm_unsteady
        self.taub = taub
        self.ub = ub
        self.theta_dofs = theta_dofs
        self.lamda_dofs = lamda_dofs
        self.un_dofs = un_dofs

    def steady_termination_criterion(self, k, diff, tol, kmax):

        out = False

        if diff < tol:
            print(CRED + "Terminating because norm unsteady < tol" + CEND)
            out = True

        if k == kmax:
            print(CRED + "Terminating because k = k_max" + CEND)
            out = True

        return out


    def unsteady_termination_criterion(self, k, diff, tol, kmax):

        out = False

        if k == kmax:
            out = True

        return out


    def save_all(self, path):

        u, p, lamda = self.z.split()

        # Save pvd
        outfile = File(path + "/output.pvd")
        outfile.write(u,p,lamda)

        # Save h5 files
        self.save_h5(path)

        # Save txt files
        self.save_files(path)

        # Plot
        self.save_plots(path)


    def save_h5(self, path):

        # recover solution
        u, p, lamda = self.z.split()
        coords = self.mesh.coordinates

        # Save solution
        h5 = HDF5File(path + "/solution.xml.gz", "w")
        h5.write(u, "/velocity")
        h5.write(p, "/pressure")
        h5.write(lamda, "/lamda")
        h5.write(coords, "/coordinates")
        del h5

    def save_files(self, path):

        # Save txts of different files
        np.savetxt(path + "/volume.txt", np.array(self.cavity_volume))
        np.savetxt(path + "/tau.txt", np.array(self.taub))
        np.savetxt(path + "/ub.txt", np.array(self.ub))
        np.savetxt(path + "/theta.txt", np.array(self.theta_dofs))
        np.savetxt(path + "/lamda.txt", np.array(self.lamda_dofs))
        np.savetxt(path + "/un.txt", np.array(self.un_dofs))

    def save_plots(self, path):

        u, p, lamda = self.z.split()
        theta = self.mesh.coordinates.dat.data[self.contact_solver.indU2Ub, 1]

        # plot lamda
        fig, ax = plt.subplots()
        lamda_value = lamda.dat.data[self.contact_solver.indMb2M]
        ax.plot(self.contact_solver.xMb, lamda_value, label = "lambda")
        ax.plot(self.contact_solver.xMb[self.contact_solver.ind_free_boundary_sorted], lamda_value[self.contact_solver.ind_free_boundary_sorted], marker = "x", markersize = 4, linewidth = 0)
        ax.legend()
        fig.savefig(path + "/lambda.png")
        plt.close(fig)

        # plot theta
        fig, ax = plt.subplots()
        ax.plot(self.contact_solver.xUb, self.bed_points, color = "black")
        ax.plot(self.contact_solver.xUb, theta)
        theta_mid = 0.5 * (theta[1:] + theta[:-1])
        x_mid = 0.5 * (self.contact_solver.xUb[1:] + self.contact_solver.xUb[:-1])
        ax.plot(x_mid[self.contact_solver.ind_free_boundary_sorted], theta_mid[self.contact_solver.ind_free_boundary_sorted], marker = "x", markersize = 4, linewidth = 0)
        fig.savefig(path + "/theta.png")
        plt.close(fig)

        # Plot cavity volume
        t = np.linspace(0, self.k * self.dt, self.k)
        fig, ax = plt.subplots()
        ax.plot(t, self.cavity_volume)
        fig.savefig(path + "/volume.png")
        plt.close(fig)

        # Plot tau
        fig, ax = plt.subplots()
        ax.plot(t, self.taub)
        fig.savefig(path + "/tau.png")
        plt.close(fig)

        # Plot ub
        fig, ax = plt.subplots()
        ax.plot(t, self.ub)
        fig.savefig(path + "/ub.png")
        plt.close(fig)

        # Plot unsteady norm
        fig, ax = plt.subplots()
        ax.plot(t, self.norm_unsteady)
        ax.set_yscale("log")
        fig.savefig(path + "/norm_unsteady.png")
        plt.close(fig)
